/**
 * Created by Hakan on 2015-02-05.
 */

var request = require('request');
var ipn = require('paypal-ipn');
// https://github.com/andzdroid/paypal-ipn

var ppParam = {
    mc_gross: '299.00',
    protection_eligibility: 'Eligible',
    address_status: 'unconfirmed',
    payer_id: '7RTCYDS9ZPS2E',
    tax: '0.00',
    address_street: 'ESpachstr. 1',
    payment_date: '09:37:02 Feb 10, 2015 PST',
    payment_status: 'Completed',
    charset: 'windows-1252',
    address_zip: '79111',
    first_name: 'Handlare',
    mc_fee: '13.42',
    address_country_code: 'DE',
    address_name: 'Handlare Omnisson',
    notify_version: '3.8',
    custom: '087284EA-F411-4D52-8588-D0CBC6EA032E',
    payer_status: 'verified',
    business: 'hakandev-facilitator@omninova.se',
    address_country: 'Germany',
    address_city: 'Freiburg',
    quantity: '1',
    verify_sign: 'AXbHfyOhp.g26Kv5HlJEYOBllEYOApQoyPfrMuugqIi937Az2cuSpsjx',
    payer_email: 'testbuyer@omninova.se',
    txn_id: '7B632076KL6656158',
    payment_type: 'instant',
    last_name: 'Omnisson',
    address_state: 'Empty',
    receiver_email: 'hakandev-facilitator@omninova.se',
    payment_fee: '',
    receiver_id: 'MUK4C7EKVD9WY',
    txn_type: 'web_accept',
    item_name: 'Wellnessplay tre månader',
    mc_currency: 'SEK',
    item_number: '512',
    residence_country: 'DE',
    test_ipn: '1',
    handling_amount: '0.00',
    transaction_subject: '087284EA-F411-4D52-8588-D0CBC6EA032E',
    payment_gross: '',
    shipping: '0.00',
    ipn_track_id: '1368ec5a43b9'
};

var bamse = {
    mc_gross: '249.00',
    protection_eligibility: 'Eligible',
    address_status: 'confirmed',
    payer_id: 'QNWKASMMDEVS2',
    tax: '0.00',
    address_street: '1 Main St',
    payment_date: '11:38:45 Feb 10, 2015 PST',
    payment_status: 'Completed',
    charset: 'windows-1252',
    address_zip: '95131',
    first_name: 'Test',
    mc_fee: '12.96',
    address_country_code: 'US',
    address_name: 'Test Buyer',
    notify_version: '3.8',
    custom: '087284EA-F411-4D52-8588-D0CBC6EA032E',
    payer_status: 'verified',
    business: 'hakandev-facilitator@omninova.se',
    address_country: 'United States',
    address_city: 'San Jose',
    quantity: '1',
    verify_sign: 'AV7Pb-dHFSgQ2DZ52OWZwDeVhBs8AOoGyt3EpXCoBN41-XgLSx8jzJPX',
    payer_email: 'paypal-buyer@omninova.se',
    txn_id: '2UE81141FL860933G',
    payment_type: 'instant',
    last_name: 'Buyer',
    address_state: 'CA',
    receiver_email: 'hakandev-facilitator@omninova.se',
    payment_fee: '',
    receiver_id: 'MUK4C7EKVD9WY',
    txn_type: 'web_accept',
    item_name: 'Wellnessplay+tre+m%E5nader',
    mc_currency: 'SEK',
    item_number: 'WP-256',
    residence_country: 'US',
    test_ipn: '1',
    handling_amount: '0.00',
    transaction_subject: '087284EA-F411-4D52-8588-D0CBC6EA032E',
    payment_gross: '',
    shipping: '0.00',
    ipn_track_id: 'ae7cf625288e'
};

var bamse_de = {
    mc_gross: '249.00',
    protection_eligibility: 'Eligible',
    address_status: 'unconfirmed',
    payer_id: '7RTCYDS9ZPS2E',
    tax: '0.00',
    address_street: 'ESpachstr. 1',
    payment_date: '11:51:05 Feb 10, 2015 PST',
    payment_status: 'Completed',
    charset: 'windows-1252',
    address_zip: '79111',
    first_name: 'Handlare',
    mc_fee: '11.72',
    address_country_code: 'DE',
    address_name: 'Handlare Omnisson',
    notify_version: '3.8',
    custom: '087284EA-F411-4D52-8588-D0CBC6EA032E',
    payer_status: 'verified',
    business: 'hakandev-facilitator@omninova.se',
    address_country: 'Germany',
    address_city: 'Freiburg',
    quantity: '1',
    verify_sign: 'AXbHfyOhp.g26Kv5HlJEYOBllEYOAkZyhlAz24NLSMZWLf1Q5uba09.q',
    payer_email: 'testbuyer@omninova.se',
    txn_id: '85P80501TG9666451',
    payment_type: 'instant',
    last_name: 'Omnisson',
    address_state: 'Empty',
    receiver_email: 'hakandev-facilitator@omninova.se',
    payment_fee: '',
    receiver_id: 'MUK4C7EKVD9WY',
    txn_type: 'web_accept',
    item_name: 'Wellnessplay+tre+m%E5nader',
    mc_currency: 'SEK',
    item_number: 'WP-256',
    residence_country: 'DE',
    test_ipn: '1',
    handling_amount: '0.00',
    transaction_subject: '087284EA-F411-4D52-8588-D0CBC6EA032E',
    payment_gross: '',
    shipping: '0.00',
    ipn_track_id: '4a05c95a818cc'
};

ipn.verify(bamse, {'allow_sandbox': true}, function callback(err, mes) {

    if (err) {
        console.error(err);
    } else {

        console.log('Yey!');
    }

});


//request.post(
//    'https://www.paypal.com/se/cgi-bin/webscr?cmd=_notify-validate',
//    {
//        json: ppParam
//    },
//    function (error, response, body) {
//
//        //console.log(unescape('Wellnessplay+en+m%E5nad'));
//
//        if (!error && response.statusCode == 200) {
//            console.log(body)
//        } else {
//            console.log(error);
//            console.log(response.statusCode);
//        }
//    }
//);