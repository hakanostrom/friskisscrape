/**
 * Created by Hakan on 2015-02-05.
 */

var http = require('http');
var https = require('https');

var options = {
    host: 'www.random.org',
    path: '/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new'
};

var callback = function (response) {
    var str = '';

    //another chunk of data has been recieved, so append it to `str`
    response.on('data', function (chunk) {
        str += chunk;
    });

    //the whole response has been recieved, so we just print it out here
    response.on('end', function () {
        console.log(str);
    });
};

//https.request(options, callback).end();


// =======

options = {
    host: 'wellnessplay.azure-mobile.net',
    path: '/api/paypal',
    method: 'GET'
};

var req = https.request(options, function(res) {
    console.log(res.statusCode);
    res.on('data', function(d) {
        process.stdout.write(d);
    });
});
req.end();

req.on('error', function(e) {
    console.error(e);
});