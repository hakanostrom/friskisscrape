/**
 * Created by Hakan on 2015-02-01.
 */

var Firebase = require("firebase");
var moment = require('moment');

var FB_SECRET = "zea5VfmD86KOPGLAOoQTqnidCt4oxTJaJgFoownO";

var fbRef = new Firebase("https://1389357167462.firebaseio.com/");

var write = function () {

    exitQueue(true);
    fbRef.child('testwrite').set(moment().millisecond(), function (error) {
        if (error) {
            console.log("Data could not be saved." + error);
        } else {
            console.log("Data saved successfully.");
        }

        exitQueue(false);
    });

    exitQueue(true);
    fbRef.child('testwrite2').set(moment().millisecond(), function (error) {
        if (error) {
            console.log("Data 2 could not be saved." + error);
        } else {
            console.log("Data 2 saved successfully.");
        }

        exitQueue(false);
    });
};

var exitQueue = function (status) {

    if (typeof exitStatus == 'undefined')
        exitStatus = 0;

    exitStatus += (!!status) * 2 - 1;

    //console.log(exitStatus);

    if (exitStatus == 0)
        process.exit();
};


fbRef.authWithCustomToken(FB_SECRET, function (error, result) {
    if (error) {
        console.log("Login Failed!", error);
    } else {
        console.log("Authenticated successfully with payload:", result.auth);
    }
});

fbRef.onAuth(function (authData) {
    if (authData) {
        console.log("Authenticated with uid:", authData.uid);

        write();

    } else {
        console.log("Client unauthenticated.")
    }
});
