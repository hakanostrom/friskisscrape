#!/usr/bin/env node

/**
 * Created by Hakan on 2015-01-29.
 */

var Firebase = require("firebase");
var Request = require("request");
var Cheerio = require("cheerio");

//var moment = require('moment');
var moment = require('moment-timezone');

var console = require('tracer').console(
    {
        format: "[{{file}}:{{line}}] {{message}}"
    });

// for Firebase: 1389357167462
//var FB_SECRET = "zea5VfmD86KOPGLAOoQTqnidCt4oxTJaJgFoownO";

// for firebase: friskis
var FB_SECRET = "wev48TbL2NvYdB5aINQ1oWklJZMDMq5yNcl4G71c";

// deprecated (?)
/*
 var exitQueue = function (status) {

 if (typeof exitStatus == 'undefined')
 exitStatus = 0;

 exitStatus += (!!status) * 2 - 1;

 //console.log(exitStatus);

 if (exitStatus == 0)
 process.exit();
 };
 */

/* Scrape info from Friskissvettis.se and save to Firebase*/

//var url = "http://sundsvall.friskissvettis.se/";

var fbRef = new Firebase("https://friskis.firebaseio.com/");


// Complete callback
var onComplete = function (error) {
    if (error) {
        console.log('Synchronization failed');
    } else {
        console.log('Synchronization succeeded');
    }
};

// Login to get write access

fbRef.authWithCustomToken(FB_SECRET, function (error, result) {
    if (error) {
        console.log("Login Failed!", error);
    } else {
        console.log("Authentication successfull");

        //process.exit();

        removeOld();
    }
});


// Remove old ones (rullande och byte)

function removeOld() {
    Request(fbRef.toString() + "/.json", function (error, response, body) {

        if (!error && response.statusCode == 200) {

            var jsonSchema = JSON.parse(body);

            // remove from json
            for (var key in jsonSchema) {
                if (key != 'extra') {
                    var dayEntry = jsonSchema[key];
                    for (var dKey in dayEntry) {
                        var placeEntry = dayEntry[dKey];
                        for (var pKey in placeEntry) {
                            var eventEntry = placeEntry[pKey];
                            if (typeof eventEntry == 'object') {

                                if (eventEntry) {
                                    delete eventEntry.rullande;
                                    delete eventEntry.byte;
                                }


                            }
                            //console.log(eventEntry);
                        }
                    }
                }
            }

            // update json to firebase
            fbRef.update(jsonSchema, function (error) {

                    parseExtrapassSingle("http://sundsvall.friskissvettis.se/schema/extra-pass-2015");
                    //parseExtrapass("http://sundsvall.friskissvettis.se/schema/extra-pass-2015");

                    //Schemaändringar
                    parseByte("http://sundsvall.friskissvettis.se/schema");

                    parseRullande();

                    fbRef.child("lastrun").set(moment().tz('Europe/Stockholm').format(), function () {

                        console.log('Set timeout to exit app after 20 sec');

                        // Exit after 20 sec
                        setTimeout(function () {
                            process.exit();
                        }, 20000);

                    });

                }
            );
        }
    });

}
// Extrapass

function parseExtrapassSingle(urlToParse) {
    Request(urlToParse, function (error, response, html) {
        if (!error) {
            var $ = Cheerio.load(html);

            var main = $('#main');

            // if doc not existing
            if (!main.html())
                return;

            // Remove heading one
            main.find('h1').remove();

            // Remove empty level two headings
            main
                .find('h2')
                .filter(function (index, item) {
                    return ($(this).text().trim() == "");
                })
                .remove();

            var texten = main.find('.sfContentBlock').html().trim();

            fbRef.child("extrapass").set(texten, onComplete);

        }

    });

}

function parseExtrapass(urlToParse) {

    Request(urlToParse, function (error, response, html) {

        if (!error) {
            var $ = Cheerio.load(html);
            var resultJson = {};

            // Is like next() but also returns the text nodes.
            // http://stackoverflow.com/questions/298750/how-do-i-select-text-nodes-with-jquery

            Cheerio.prototype.nextNode = function () {
                var contents = $(this).parent().contents();
                return contents.get(contents.index(this) + 1);
            };

            $('#main').find('.sfContentBlock').filter(function () {

                // title
                var data = $("h1");

                resultJson.title = data.text();
                resultJson.events = [];

                $(this).find("strong").each(function (item) {

                    try {

                        var dateData = $(this).text().trim();
                        var textData = $(this).nextNode().nodeValue.trim();


                        //console.log(dateData);
                        //console.log(textData);

                        if (dateData != "") {
                            //console.log(textData.match("([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9] - ([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]"));

                            var jsonEvent = {};
                            jsonEvent.date = dateData;
                            //jsonEvent.time = textData.match("([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9] - ([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]")[0];
                            jsonEvent.time = "";
                            //jsonEvent.text = textData.replace(jsonEvent.time, "").substr(1).trim();
                            jsonEvent.text = textData.trim();

                            resultJson.events.push(jsonEvent);
                        }

                    } catch (err) {
                        console.log('now error:', err);
                    }

                });

                fbRef.child("extra").set(resultJson.events);
            });
        }
    });

}

// Ändringar (byte)

function parseByte(urlToParse) {

    Request(urlToParse, function (error, response, html) {

        if (!error) {
            var $ = Cheerio.load(html);

            $('#main').find('.sfContentBlock h2').each(function (index, item) {

                var text = $(item).text();

                if (text.indexOf("Jympa") > -1) {
                    //console.log(' -=- Jympa-ändringar -=- ');

                    $(item).parent().find("table tr").filter(function () {
                        return $(this).text().indexOf("Pass som byts ut") <= -1;
                    }).each(function () {
                        //console.log($(this).text());

                        var event = {};
                        event.date = ($($(this).find("td")[0]).text().match("[123]?[0-9]\/[1-9]|1[012]")[0]);
                        event.from = ($($(this).find("td")[1]).text());
                        event.to = ($($(this).find("td")[2]).text());

                        moment.locale('sv');

                        var dateData = $($(this).find("td")[0]).text();
                        var time = (moment(dateData, 'D/M HH:mm'));
                        var day = (time.locale('en').format('dddd').toLowerCase());

                        event.date = time.format('D/M');

                        var doSnap = function (snapshot) {
                            var data = snapshot.val();
                            if (data != null && (event.from.indexOf(data.coach) > -1 || data.coach.toLowerCase().indexOf('rullande') > -1)) {
                                var p = snapshot.ref().child('byte').push(event);
                                //console.log('added change!', p.key(), event);
                            }
                        };

                        var eventRefLower = fbRef.child(day).child("lower").orderByChild("time").startAt(time.format('HH:mm')).limitToFirst(1);
                        eventRefLower.once("child_added", function (snapshot) {
                            doSnap(snapshot);
                            //console.log('doing snap on ', snapshot.val());
                        });

                        var eventRefUpper = fbRef.child(day).child("upper").orderByChild("time").startAt(time.format('HH:mm')).limitToFirst(1);
                        eventRefUpper.once("child_added", function (snapshot) {
                            doSnap(snapshot);
                        });
                    });
                }
                if (text.indexOf("Spinning") > -1) {
                    //console.log(' -=- Spinning-ändringar -=- ');

                    $(item).parent().find("table tr").filter(function () {
                        return $(this).text().indexOf("Pass som byts ut") <= -1;
                    }).each(function () {
                        //console.log($(this).text());

                        try {

                            var event = {};
                            event.date = ($($(this).find("td")[0]).text().match("[123]?[0-9]\/[1-9]|1[012]")[0]);
                            event.from = ($($(this).find("td")[1]).text());
                            event.to = ($($(this).find("td")[2]).text());

                            moment.locale('sv');

                            var dateData = $($(this).find("td")[0]).text();
                            var time = (moment(dateData, 'D/M HH:mm'));

                            event.date = time.format('D/M');
                            //console.log('Fetch spinning time: ', event.date);


                            var day = (time.locale('en').format('dddd').toLowerCase());

                            var doSnap = function (snapshot) {
                                var data = snapshot.val();
                                if (data != null && (event.from.indexOf(data.coach) > -1 || data.coach.toLowerCase().indexOf('rullande') > -1)) {
                                    var p = snapshot.ref().child('byte').push(event);
                                    //console.log('added change!', p.key(), event);
                                }
                            };

                            var eventRefLower = fbRef.child(day).child("spin").orderByChild("time").startAt(time.format('HH:mm')).limitToFirst(1);
                            eventRefLower.once("child_added", function (snapshot) {
                                doSnap(snapshot);
                            });


                        } catch (err) {
                        }


                    });
                }
                if (text.indexOf("Cirkelgym/HIT") > -1) {
                    //console.log(' -=- Cirkelgym/HIT-ändringar -=- ');


                    try {

                        $(item).parent().find("table tr").filter(function () {
                            return $(this).text().indexOf("Pass som byts ut") <= -1;
                        }).each(function () {
                            //console.log($(this).text());

                            var event = {};
                            event.date = ($($(this).find("td")[0]).text().match("[123]?[0-9]\/[1-9]|1[012]")[0]);
                            event.from = ($($(this).find("td")[1]).text());
                            event.to = ($($(this).find("td")[2]).text());

                            moment.locale('sv');

                            var dateData = $($(this).find("td")[0]).text();
                            var time = (moment(dateData, 'D/M HH:mm'));
                            var day = (time.locale('en').format('dddd').toLowerCase());

                            event.date = time.format('D/M');

                            var doSnap = function (snapshot) {
                                var data = snapshot.val();
                                if (data != null && (event.from.indexOf(data.coach) > -1 || data.coach.toLowerCase().indexOf('rullande') > -1)) {
                                    var p = snapshot.ref().child('byte').push(event);
                                    //console.log('added change!', p.key(), event);
                                }
                            };

                            var eventRefLower = fbRef.child(day).child("gym").orderByChild("time").startAt(time.format('HH:mm')).limitToFirst(1);
                            eventRefLower.once("child_added", function (snapshot) {
                                doSnap(snapshot);
                            });


                            //eventRefLower.once("child_added", function (snapshot) {
                            //    console.log(' ----- child_added ----- ');
                            //    console.log(snapshot.val());
                            //});
                            //eventRefLower.once("value", function (snapshot) {
                            //    console.log(' ----- value ----- ');
                            //    console.log(snapshot.val()[0]);
                            //});


                        });


                    } catch (err) {

                    }


                }
            });
        }
    });

}

// Rullande

function parseRullande(urlToParse) {

    // Spinning

    Request("http://sundsvall.friskissvettis.se/schema/rullande-spinnpass-ht-2015", function (error, response, html) {

        if (!error) {
            var $ = Cheerio.load(html);


            $('#main').find('.sfContentBlock h2')
                .filter(function () {
                    return $(this).text().trim() != "";
                })
                .each(function (index, item) {

                    var text = $(item).text();

                    //console.log(text);

                    var rulla = [];

                    // ej tabell
                    $(item).nextAll('p')
                        .each(function (index, item) {
                            //console.log($(item).text());

                            var plupp = $(item).text().split(':');

                            rulla.push({
                                "date": plupp[0],
                                "coach": plupp[1]
                            });

                        });

                    // tabell
                    $(item).nextAll('table')
                        .find('tr')
                        .filter(function (index, item) {
                            return index;
                        })
                        .each(function (index, item) {
                            var date = $(item.childNodes[1]).text();
                            var coach = $(item.childNodes[7]).text();
                            var full = $(item.childNodes[3]).text() + " " + $(item.childNodes[5]).text() + " " + $(item.childNodes[7]).text();

                            var momentDate = moment(date.trim(), 'DD/MM');
                            if (!momentDate.isValid())
                                momentDate = moment(date.trim(), 'MMMM').add(1, 'M');
                            else
                                momentDate = momentDate.add(1, 'day');


                            if (momentDate.isAfter() && date.trim())
                                if (coach != "") {
                                    rulla.push({
                                        "date": date,
                                        "coach": coach
                                    });
                                } else {
                                    rulla.push({
                                        "date": date,
                                        "coach": full
                                    });
                                }
                        });

                    moment.locale('sv');

                    var eventDate = moment(text.toLowerCase(), 'ddd HH:mm');
                    var day = (eventDate.locale('en').format('dddd').toLowerCase());

                    // debug
                    //console.log(text.toLowerCase());
                    //console.log(eventDate.format('HH:mm'));
                    //console.log(' --- ');

                    // lägg in
                    var eventRefLower = fbRef.child(day).child("spin").orderByChild("time").startAt(eventDate.format('HH:mm')).limitToFirst(1);

                    eventRefLower.once("child_added", function (snapshot) {

                        //console.log(snapshot.ref().toString());
                        //console.log(snapshot.child('time').val());
                        //console.log(eventDate.format('HH.mm'));
                        //console.log(snapshot.child('time').val().indexOf('06'));

                        snapshot.ref().child('rullande').set(rulla);
                    });
                });
        }
    });

    // jympa

    Request("http://sundsvall.friskissvettis.se/schema/rullande-jympapass-ht-2015", function (error, response, html) {

        if (!error) {
            var $ = Cheerio.load(html);

            var inLower = true;


            $('#main').find('.sfContentBlock h2')
                .filter(function () {
                    return $(this).text().trim() != "";
                })
                .each(function (index, item) {

                    var text = $(item).text();

                    //if (text.toLowerCase().indexOf('cirkelgym') > -1) {
                    //    console.log(text);
                    //
                    //    // break out of each-loop
                    //    return false;
                    //}


                    var rulla = [];

                    // ej tabell
                    $(item).nextAll('p')
                        .each(function (index, item) {
                            //console.log($(item).text());

                            var plupp = $(item).text().split(':');

                            rulla.push({
                                "date": plupp[0],
                                "coach": plupp[1]
                            });

                        });

                    // tabell
                    $(item).nextAll('table')
                        .find('tr')
                        .filter(function (index, item) {
                            return index;
                        })
                        .each(function (index, item) {
                            var date = $(item.childNodes[1]).text();
                            var coach = $(item.childNodes[5]).text() + " " + $(item.childNodes[7]).text();
                            var full = $(item.childNodes[3]).text() + " " + $(item.childNodes[7]).text() + " " + $(item.childNodes[5]).text();

                            var momentDate = moment(date.trim(), 'DD/MM');
                            if (!momentDate.isValid())
                                momentDate = moment(date.trim(), 'MMMM').add(1, 'M');
                            else
                                momentDate = momentDate.add(1, 'day');

                            if (momentDate.isAfter() && date.trim())
                                if (coach != "") {
                                    rulla.push({
                                        "date": date,
                                        "coach": coach
                                    });
                                } else {
                                    rulla.push({
                                        "date": date,
                                        "coach": full
                                    });
                                }
                        });

                    moment.locale('sv');

                    var eventDate = moment(text.toLowerCase(), 'ddd HH:mm');
                    var day = (eventDate.locale('en').format('dddd').toLowerCase());

                    // only to whom it concerns

                    if (text.toLowerCase().indexOf('cirkelgym') > -1) {
                        inLower = false;
                    }


                    // exclude headlines that is not timestamps
                    if (text.toLowerCase().indexOf('cirkelgym') < 0) {
                        if (inLower) {
                            // lägg in i lower
                            var eventRefLower = fbRef.child(day).child("lower").orderByChild("time").startAt(eventDate.format('HH:mm')).limitToFirst(1);
                            eventRefLower.once("child_added", function (snapshot) {
                                snapshot.ref().child('rullande').set(rulla);
                            });
                        } else {
                            // lägg in i gym
                            var eventRefLower = fbRef.child(day).child("gym").orderByChild("time").startAt(eventDate.format('HH:mm')).limitToFirst(1);
                            eventRefLower.once("child_added", function (snapshot) {
                                //console.log(text);
                                //console.log(eventDate.format('HH:mm'));
                                //console.log(snapshot.val());
                                snapshot.ref().child('rullande').set(rulla);
                            });
                        }
                    }
                });
        }
    });

    // Cirkelgym

    // http://sundsvall.friskissvettis.se/schema/rullande-cirkelgym-ht-2015

    Request("http://sundsvall.friskissvettis.se/schema/rullande-cirkelgym-ht-2015", function (error, response, html) {

        if (!error) {
            var $ = Cheerio.load(html);

            $('#main').find('.sfContentBlock h2')
                .filter(function () {
                    return $(this).text().trim() != "";
                })
                .each(function (index, item) {

                    // break out of each-loop
                    //return false;

                    var text = $(item).text();

                    //console.log(text);

                    var rulla = [];

                    // ej tabell
                    $(item).nextAll('p')
                        .each(function (index, item) {
                            //console.log($(item).text());

                            var plupp = $(item).text().split(':');

                            rulla.push({
                                "date": plupp[0],
                                "coach": plupp[1]
                            });

                        });

                    // tabell
                    $(item).nextAll('table')
                        .find('tr')
                        .filter(function (index, item) {
                            return index;
                        })
                        .each(function (index, item) {
                            var date = $(item.childNodes[1]).text();
                            var coach = $(item.childNodes[7]).text();
                            var full = $(item.childNodes[3]).text() + " " + $(item.childNodes[5]).text();


                            var momentDate = moment(date.trim(), 'DD/MM');
                            if (!momentDate.isValid())
                                momentDate = moment(date.trim(), 'MMMM').add(1, 'M');
                            else
                                momentDate = momentDate.add(1, 'day');


                            if (momentDate.isAfter() && date.trim())
                                if (coach != "") {
                                    rulla.push({
                                        "date": date,
                                        "coach": coach
                                    });
                                } else {
                                    rulla.push({
                                        "date": date,
                                        "coach": full
                                    });
                                }
                        });

                    moment.locale('sv');

                    var eventDate = moment(text.toLowerCase(), 'ddd HH:mm');
                    var day = (eventDate.locale('en').format('dddd').toLowerCase());


                    // lägg in
                    var eventRefLower = fbRef.child(day).child("gym").orderByChild("time").startAt(eventDate.format('HH:mm')).limitToFirst(1);
                    eventRefLower.once("child_added", function (snapshot) {
                        snapshot.ref().child('rullande').set(rulla);
                    });
                });
        }
    });


}
